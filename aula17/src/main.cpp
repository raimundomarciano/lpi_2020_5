#include <iostream>
#include <vector>
#include "../include/agency.h"
#include "../include/fila.h"
#include "../include/clientepj.h"
using namespace std;

int main(int argc, char* argv[]){

    Clientepj eu;
    Clientepj tu;
    Clientepj ele;
    Clientepj ela;
    eu.setRazaoSocial("Eu");
    tu.setRazaoSocial("Tu");
    ele.setRazaoSocial("Ele");
    ela.setRazaoSocial("Ela");

    Cliente nos;
    Cliente vos;
    Cliente eles;
    Cliente elas;
    nos.setNome("Nós");
    vos.setNome("Vós");
    eles.setNome("Eles e Elas");

    Fila<Clientepj> empresarial(3);
    empresarial.push(eu);
    empresarial.push(tu);
    empresarial.push(ele);
    empresarial.push(ela);
    cout << empresarial.back().getRazaoSocial() << endl;

    Fila<Cliente> gente(2);
    gente.push(nos);
    gente.push(vos);
    gente.push(eles);
    
    /*
    cout << gente.back)().getNome() << endl;
    cout << gente.back)().getNome() << endl;
    cout << gente.back)().getNome() << endl;
    */
    cout << gente.back().getNome() << endl;
    gente.pop();
    cout << gente.back().getNome() << endl;
    return 0;
}