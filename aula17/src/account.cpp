#include "../include/account.h"

int Conta::quantidadeContas;

Conta::Conta(Cliente titular) : numeroConta(0), saldo(0), titular(titular){
        this->quantidadeContas += 1;
}
Conta::Conta() : numeroConta(0), saldo(0){
        this->quantidadeContas += 1;
}

Conta::~Conta(){}

void Conta::saca(int valor){
    saldo -= valor;
}

void Conta::deposita(int valor){
    saldo += valor;
}