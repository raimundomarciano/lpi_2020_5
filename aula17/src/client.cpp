#include "../include/client.h"
#include <string>

int Cliente::quantidadeClientes;

Cliente::Cliente() : nome(""), cpf(""){
    this->quantidadeClientes += 1;
}

void Cliente::setNome(string novoNome){
    nome = novoNome;
}

string Cliente::getNome(){
    return nome;
}

void Cliente::setCpf(int novoCpf){
    cpf = novoCpf;
}

string Cliente::getCpf(){
    return cpf;
}