#ifndef clientepf
#define clientepf

#include <iostream>
#include <string>
using namespace std;

class Cliente {
    private:
        string nome;
        string cpf;
    public:
        Cliente();     
        static int quantidadeClientes;
        void setNome(string novoNome);
        string getNome();
        void setCpf (int cpf);
        string getCpf();
};

#endif