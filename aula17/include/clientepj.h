#ifndef clientepj
#define clientepj

#include <string>
#include "client.h"
using namespace std;

class Clientepj{    
    private:
        string razaoSocial;
        string cnpj;
    public:
        Clientepj();
        static int quantidadeClientesPJ;
        void setRazaoSocial(string razao);
        string getRazaoSocial();
        void setCnpj(string novoCnpj);
        string getCnpj();
};

#endif