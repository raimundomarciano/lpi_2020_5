#include "account.h"
#include <vector>

class Agencia {
    public:
    vector<Conta> rolContas;
    string nomeAgencia;
    unsigned numeroAgencia;

    void transfere(int contaOrigem, int contaDestino, double valor);
    Conta localizaConta(int contaProcurada);

};