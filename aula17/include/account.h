#include <iostream>
#include <string>
#include "client.h"

class Conta {
    public:
        Conta(Cliente titular);
        Conta();
        ~Conta();

        int numeroConta;
        double saldo;
        Cliente titular;
        static int quantidadeContas;

        void saca(int valor);
        void deposita(int valor);

};