#include <iostream>
using namespace std;
// não é possível separar a implementação da classe genérica de seu cabeçalho
// logo, não é necessário o fila.cpp


template< class Q >
// define o template Q de Queue (fila)

class Fila{
    private:
        int size; // tamanho max da fila
        int top; // contador do topo
        Q* elements; // array de elementos de algum tipo

    public:
        Fila(int s){
            this->size = s; // o construtor define o tamanho max da fila
            this->top = -1; // indica que n tem nada ainda; 1st elemento é top = 0
            this->elements = new Q[size]; // constroi um array de size elementos com base no template
        }

        void push(Q element){
            if(top == (size - 1)){
                cout << "Fila cheia" << endl;
                // se o topo chegar a s-1, só printe "fila cheia"
            } else {
                this->elements[++top] = element;
                // caso contrario, adicione o elemento a posicao topo+1 
            }
        }

        void pop(){
            if (top == -1){
                cout << "Fila vazia" << endl;
                // se o topo estiver em -1, não tem o que tirar
            } else {
                this->top--;
                // não efetivamente remove, só muda o contador de posição
            }
        }

        bool isEmpty(){
            if (top == -1) {
                return true; 
            } else {
                return false;
            }
        }
        Q back(){
            return this->elements[top];
            //retorna o elemento que está no contador de topo
            //não necessariamente o último listado no array
        }
};