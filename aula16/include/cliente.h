#ifndef CLI_H
#define CLI_H

#include <vector>
#include <string>
#include "produto.h"
using namespace std;

class Cliente{
    public:
        Cliente();
        string nomeCliente;
        int idCliente;
        static int qtdCliente;
        double saldo;
        vector<Produto> sacola;
        void compra(string produto, double preco);
        void verSacola();
        void registro();

};


/*
A classe Cliente deve ter os atributos: saldo, sacola (vector de string);
•No saldo vai conter quanto o cliente tem em dinheiro e pode gastar em compras;
•Na sacola vai conter todas as compras do cliente;
A classe Cliente deve ter os métodos: 
compra (recebe o produto e o preço, 
o método deve verificar se o cliente tem saldo suficiente para comprar 
o produto,se não, emite um aviso, se sim, faz a compra, diminui o saldo 
e adiciona o produto na sacola), verSacola (mostra o conteúdo da sacola) 
e  registro(esse método escreve o conteúdo da sacola em um arquivo 
chamado cliente_x.txt, onde para cada cliente dever um arquivo próprio 
mudando x para 1, 2, 3, etc. Até a quantidade de clientes existentes;

*/

#endif