#ifndef PRO_H
#define PRO_H

#include <string>
#include <regex>

class Produto {
    public:
        // não é necessário construtor nessa classe

        int codigo;
        std::string produto;
        std::string unidadeMedida;
        double preco;
        int quantidade;

};

#endif