#ifndef ESTAB_H
#define ESTAB_H

#include <string>
#include "produto.h"
using namespace std;

class Estabelecimento {
public:
    Estabelecimento();

    int codigo;
    vector<Produto> produto;
    string unidadeMedida;
    double preco;

    void carregarEstoque();
    void listar();
    int venda(int codigo);
    void caixa();
};



/*
A classe Estabelecimento deverá consultar os produtos 
através do arquivo estoque.csv. A classe também deve gerar 
um arquivo de saída chamado caixa.csv, esse arquivo deve 
conter os produtos vendidos e o total de dinheiro 
ganho nas vendas;
O arquivo caixa.csv deve conter o código do produto, nome, 
preço, a quantidade vendida de cada produto e o total de vendas;

A classe Estabelecimento deve possuir os seguintes atributos: 
codigo, produto (lista de string), unidadeMedida, preco;

A classe Estabelecimento deve possuir os seguintes métodos: 
* listar (lista os produtos disponíveis), 
* venda (recebe como 
parâmetro o código do produto, verifica se ainda existe esse 
produto no estoque, se não retorna uma mensagem de insucesso, 
se sim, retorna uma mensagem de sucesso e escreve no arquivo 
de saída a venda efetuada), 
caixa (retorna tudo o que  foi vendido e o total ganho);
*/

#endif