#include "company.h"
#include "worker.h"
#include <iostream>
#include <vector>
using namespace std;

int main(){
    Empresa modelo;
    int quantidadeFuncionarios;
    vector<Funcionario> registroEmpregados;

    cout << "Quantos funcionários você deseja cadastrar?" << endl;
    cin >> quantidadeFuncionarios;

    for (int i = 0; i < quantidadeFuncionarios; i++){
        Funcionario temp;

        cout << "Informe o nome do funcionário: " << endl;
        string nome;
        cin >> nome;
        temp.setNome(nome);
        
        cout << "Informe o salário do funcionário: " << endl;
        double sal;
        cin >> sal;
        temp.setSalario(sal);

        temp.setDataAdmissao("13-07-2020");
        temp.setDepartamento("Geral");

        registroEmpregados.push_back(temp);
    }

    cout << "Funcionários Registrados: " << endl;
    for (size_t i = 0; i < registroEmpregados.size(); i++){
        cout << registroEmpregados[i].getNome() << " " <<
        "R$ " << registroEmpregados[i].getSalario() << endl;
    }
    
    cout << endl << "Processando um aumento de 10% nos salários..." << endl;
    for (size_t i = 0; i < registroEmpregados.size(); i++){
        double sal = registroEmpregados[i].getSalario();
        sal = sal * 1.1;
        registroEmpregados[i].setSalario(sal);
    }

     cout << "Registros atualizados... " << endl;
    for (size_t i = 0; i < registroEmpregados.size(); i++){
        cout << registroEmpregados[i].getNome() << " " <<
        "R$ " << registroEmpregados[i].getSalario() << endl;
    }



    return 0;
}