#include "worker.h"
#include <string>

void Funcionario::setNome(string nome){
    this->nome = nome;
};
string Funcionario::getNome(){
    return this->nome;
};

void Funcionario::setSalario(double salario){
    this->salario = salario;
};
double Funcionario::getSalario(){
    return this->salario;
};

void Funcionario::setDataAdmissao(string admissao){
    this->dataAdmissao = admissao;
};
string Funcionario::getDataAdmissao(){
    return this->dataAdmissao;
};

void Funcionario::setDepartamento(string departamento){
    this->departamento = departamento;
};
string Funcionario::getDepartamento(){
    return this->departamento;
};