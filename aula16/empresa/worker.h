#include <string>
using namespace std;

class Funcionario {
    private:
        string nome;
        double salario;
        string dataAdmissao;
        string departamento;

    public:
        void setNome(string nome);
        string getNome();
        void setSalario(double salario);
        double getSalario();
        void setDataAdmissao(string admissao);
        string getDataAdmissao();
        void setDepartamento(string departamento);
        string getDepartamento();
};