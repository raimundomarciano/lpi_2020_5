#include <string>
using namespace std;

class Empresa {
    private:
        string denominacao;
        string cnpj;
    
    public:
        void setDenominacao(string nomeEmpresarial);
        string getDenominacao();
        void setCnpj(string cnpj);
        string getCnpj();
};