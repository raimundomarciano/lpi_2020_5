#include "company.h"

void Empresa::setDenominacao(string nomeEmpresarial){
    this->denominacao = nomeEmpresarial;
};
string Empresa::getDenominacao(){
    return this->denominacao;
};
void Empresa::setCnpj(string cnpj){
    this->cnpj = cnpj;
};
string Empresa::getCnpj(){
    return this->cnpj;
};