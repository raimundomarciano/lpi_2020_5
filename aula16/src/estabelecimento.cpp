#include "../include/estabelecimento.h"
#include "../include/cliente.h"
#include "../include/produto.h"
#include <fstream>
#include <sstream>
#include <iostream>
#include <vector>
using namespace std;

Estabelecimento::Estabelecimento(){};

void Estabelecimento::carregarEstoque(){
// temp, word e row são usadas apenas para tratar o que vem
    // do arquivo 'estoque.csv'
    string temp;
    string word;
    vector<string> row; 
    // o contador é pra pular a primeira linha do arquivo
    int contador = 0;
    // abre a conexão com o arquivo 'estoque.csv'
    ifstream filein;
    filein.open("estoque.csv", ios::in);
    

    while(!filein.eof()){
            // limpa a variavel row
            row.clear(); 
            // pega um linha de 'estoque.csv' e joga na variavel temp
            getline(filein, temp);
            // faz tipo uma quebra na string
            stringstream ss(temp);
            // pega um pedaço da ss (até a próxima ',') e põe na var word
            // depois, adiciona ao vetor row
            while (getline(ss, word, ',')) { 
                row.push_back(word); 
            } 

            // o contador só será zero se for a primeira linha do arquivo
            if (contador != 0) {    
            
            /* por conta do valor em R$, não foi trivial capturar o preço da
            mercadoria e tratar diretamente como uma variável do tipo double.
            Por isso, peguei todas as partes como strings. No caso do código e 
            da quantidade, fiz uma conversão simples para integer. No caso do preço,
            removi o ' "R$ ' no começo e a ' " ' no final, para depois converter 
            em integer e então dividir por 100 para reposicionar a vírgula que acabou
            sumindo no processo */

            Produto lendo; 
            string toint; // recebe o preco ainda em string p/ ser tratado

            lendo.codigo = stoi(row[0]);
            lendo.produto = row[1];
            lendo.unidadeMedida = row[2];
            toint = row[3];  // devido a virgula separadora de decimais, é preciso
            toint += row[4]; // pegar a parte antes[3] e depois[4] da vírgula
            toint.erase(0,4);
            toint.erase(toint.size()-1,1);
            lendo.preco = stoi(toint);
            lendo.preco = lendo.preco / 100; // recuperando a vírgula
            lendo.quantidade = stoi(row[5]);

            produto.push_back(lendo); // com isso, não é necessário um 'return produto'
        
        } else {
            // se for a primeira linha, adicione um ao contador
            contador += 1;
        }
    }
    filein.close();
}

void Estabelecimento::listar(){
    
    // além de gravar na memória, também imprime os resultados no terminal
    for (size_t i = 0; i < produto.size()-1; i++){
        cout << produto[i].codigo << " ";
        cout << produto[i].produto << "\t";
        cout << produto[i].unidadeMedida << "\t";
        cout << "R$ ";
        cout << produto[i].preco << "\t ";
        cout << produto[i].quantidade << endl;
    }

};

int Estabelecimento::venda(int codigo){
    vector<Produto> saldao;
    int temProduto = 0;
    double preco = 0;
    string nomeProduto;

    for (size_t i = 0; i < produto.size(); i++){
        if (produto[i].codigo == codigo){
            if (produto[i].quantidade > 0) {
                preco = produto[i].preco;
                nomeProduto = produto[i].produto;
                temProduto = 1;
            } else {
                cout << "Estamos sem essa mercadoria em estoque." << endl;
                return 0;
            }
        }
    }

    if (temProduto == 1){        
        Produto temp;
        ifstream filein;
        filein.open("caixa.csv", ios::app);
        string word;
        string linha;
        vector<string> row;
        int vazio = 0;
        int found = 0;
        
        while(!filein.eof()){
            row.clear(); 
            getline(filein, linha);
            stringstream ss(linha);
            
            while (getline(ss, word, ',')) { 
                row.push_back(word); 
            }

            if (row.size() != 0){
                vazio += 1;
                temp.codigo = stoi(row[0]);
                temp.produto = row[1];
                temp.preco = stod(row[2]);
                temp.quantidade = stoi(row[3]);
                if (temp.codigo == codigo){
                    temp.quantidade += 1;
                    found += 1;
                }

            saldao.push_back(temp);
            }
        }

        if (vazio == 0){
            // nao incrementou em linha alguma, então não tem nada
            temp.quantidade = 1;
            temp.codigo = codigo;
            temp.produto = nomeProduto;
            temp.preco = preco;
            saldao.push_back(temp);
        } else if (found == 0) {
            temp.quantidade = 1;
            temp.codigo = codigo;
            temp.produto = nomeProduto;
            temp.preco = preco;
            saldao.push_back(temp);
        }   
    filein.close();
    }

    ofstream fileout;
    fileout.open("caixa.csv", ios::out);

    for (size_t i = 0; i < saldao.size(); i++){

    fileout << to_string(saldao[i].codigo) << ",";
    fileout << saldao[i].produto << ",";
    fileout << saldao[i].preco << ",";
    fileout << saldao[i].quantidade << endl;
    }


    fileout.close();
			
    return 0;
}

void Estabelecimento::caixa(){
    // Supondo um caixa.csv composto de:
    // codigo,produto,preco,quantidade
    // é a saída esperada de Estabelecimento::venda()
    ifstream filein("caixa.csv", ios::in);
    string word;
    string linha;
    double total;
    vector<string> row;
            
    while(!filein.eof()){
    row.clear(); 
    getline(filein, linha);
    stringstream ss(linha);
    
    while (getline(ss, word, ',')) { 
        row.push_back(word); 
    }   

    total += stod(row[2]) * stod(row[3]);
    cout << "Produto: \t" << row[1] << 
    "\t Preço: R$ " << row[2] << 
    "\t Und Vendidas: " << row[3] << endl; 
    }
    cout << "Total: R$ " << total << endl;
}

void Cliente::compra(string produto, double preco){
    
    if (saldo < preco){
        cout << "Saldo Insuficiente." << endl;
    } else {
        cout << "Compra autorizada." << endl;
        saldo -= preco;
        cout << "Novo saldo: " << saldo << endl;

        if (sacola.size() == 0){
            Produto novo;
            novo.produto = produto;
            novo.quantidade = 1;
            novo.preco = preco;
            sacola.push_back(novo);
        } else if (sacola.size() != 0){
            int produtoExiste = 0;
            for (size_t i = 0; i < sacola.size(); i++){
                if (sacola[i].produto == produto){
                    sacola[i].quantidade += 1;
                    produtoExiste += 1;
                    break;
                }
            }
            if (produtoExiste == 0){
                Produto novo;
                novo.produto = produto;
                novo.quantidade = 1;
                novo.preco = preco;
                sacola.push_back(novo);
            }
        }

        cout << endl;
    }
}