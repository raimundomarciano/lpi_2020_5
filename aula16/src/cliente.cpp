#include "../include/cliente.h"
#include <fstream>
#include <iostream>

int Cliente::qtdCliente;

Cliente::Cliente(): saldo(0) {
    this->qtdCliente += 1;
    this->idCliente = qtdCliente;
}

    // Cliente::compra() está implementado em estabelecimento.cpp

    void Cliente::verSacola(){
        std::cout << endl << "Produtos na Sacola:" << endl;
        for (size_t i = 0; i < sacola.size(); i++){
            std::cout << sacola[i].produto;
            std::cout << " x" << sacola[i].quantidade << endl;
        }
        std::cout << endl;

    };
    void Cliente::registro(){
        string nomeArquivo="cliente_";
        nomeArquivo += to_string(idCliente);
        nomeArquivo += ".txt";

        ofstream fileout;
        fileout.open(nomeArquivo, ios::out);
        for (size_t i = 0; i < sacola.size(); i++){
            fileout << sacola[i].produto;
            fileout << " x" << sacola[i].quantidade;
            fileout << endl;
        }
        
        fileout.close();
    };