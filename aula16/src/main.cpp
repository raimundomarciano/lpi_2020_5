#include <iostream>
#include <regex>
#include <string>
#include "../include/estabelecimento.h"
#include "../include/cliente.h"
#include "../include/produto.h"
#include <sstream>

using namespace std;

int main(int argc, char* argv[]){
    Estabelecimento matriz;
    matriz.carregarEstoque();
    
    int novoCliente = -1;
    while (novoCliente != 0){
        cout << "Bem vindo, cliente!" << endl;
        Cliente primeiro;
        int continuar = -1;
        
        while (continuar != 0){
            cout << "1. Informar saldo" << endl;
            cout << "2. Ver nosso portfólio" << endl;
            cout << "3. Ver sacola" << endl;
            cout << "4. Adicionar produto na sacola" << endl;
            cout << endl;
            cout << "0. Encerrar compras." << endl;
            int opcao;
            cin >> opcao;

            if (opcao == 1){
                cout << "Digite o novo saldo: " << endl;
                cin >> primeiro.saldo;
                cout << "O novo saldo é: " << primeiro.saldo << endl;
            } else if (opcao == 2) {
                matriz.listar();
            } else if (opcao == 3) {
                primeiro.verSacola();
            } else if (opcao == 4) {
                Produto temp;
                string objeto;
                int contador = 0;
                cout << "Informe o nome ou o código do produto que deseja adquirir: " << endl;
                cin >> objeto;
                for (size_t i = 0; i < matriz.produto.size(); i++){
                    if (matriz.produto[i].produto == objeto){
                        temp = matriz.produto[i];
                        cout << temp.produto;
                        contador += 1;
                    }
                }
                
                if (contador == 0){
                    contador = stoi(objeto);
                    for (size_t i = 0; i < matriz.produto.size(); i++){
                        if (matriz.produto[i].codigo == contador){
                            temp = matriz.produto[i];
                        }
                    }
                }
                    
                primeiro.compra(temp.produto, temp.preco);
                
            } else if (opcao == 5) {
                cout << "O saldo atual é: " << primeiro.saldo << endl;
            } else if (opcao == 6){
                matriz.caixa();
            } else if (opcao == 0){
                
                continuar = 0;
                
                if (primeiro.sacola.size() != 0){  
                    
                    for (size_t i = 0; i < primeiro.sacola.size(); i++){
                        for (size_t j = 0; j < matriz.produto.size(); j++){
                            if (primeiro.sacola[i].produto == matriz.produto[j].produto){
                                primeiro.sacola[i].codigo = matriz.produto[j].codigo;
                            }
                        }
                        
                        matriz.venda(primeiro.sacola[i].codigo);
                    }
                    
                    primeiro.registro();
                }
            }
        }
      
        cout << "Deseja inicializar um novo cliente? (S/N)" << endl;
        string proxCliente;
        cin >> proxCliente;
        if (proxCliente == "N" ){
            cout << "Obrigado por comprar conosco." << endl;
            return 0;
        }
    
    }
    return 0;
}