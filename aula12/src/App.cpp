#include "../include/App.h"
#include "../include/Message.h"
#include "../include/Diary.h"
#include "../include/addTimeDate.h"
#include <iostream>
#include <fstream>
#include <regex>
#include <vector>

App::App(const std::string &filename) : diary(filename) {}

int App::run(int argc, char* argv[]) 
{
    if (argc == 1){
        return show_usage();
    }
 
    std::string action = argv[1];

    if (action == "add"){
        if (argc == 2){
            add();
            
        } else {
            add(argv[2]);
        }

    } else if  (action == "list"){
        
    } else if (action == "read") {
        readFromFile(diary.filename);
        listFromFile();
    } else if  (action == "search") {
        if (argc == 2){
            search();   
        } else {
            search(argv[2]);
        }
    } else {
        return show_usage();
    }
    
    return 0;
}

void App::add(){
    std::string message;
    std::cout << "Enter your message: " << std::endl;
    std::getline(std::cin, message);
    diary.add(message);
    App::add_another();
}

void App::add(const std::string message){
    diary.add(message);
    App::add_another();
}

void App::add_another(){
    std::cout << "Add another message or type <:q> to exit." << std::endl;
    std::string another = "n";
    std::getline(std::cin, another);
    if (another == ":q"){
        App::write(diary.filename);
        App::listFromFile();
        return;
    } else {
        App::add(another);
           
    }
}

int App::listFromFile(){
    for (size_t i = 0; i < diary.messages.size(); i++){
        const Message &messageToList = diary.messages[i];
        std::cout << 
        messageToList.time.hour << ":" <<
        messageToList.time.minute << ":" << 
        messageToList.time.second << "" << 
        messageToList.content << std::endl;
    }

    return 0;
}

int App::show_usage(){
    std::cout << "Comandos aceitos: <add>|<read>|<search>" << std::endl;
    return 1; 
}

void App::write(std::string filename){
    // writes the messages in a file
    std::ifstream filein;
    std::string mensagem;
    std::string data = "# " + get_current_date();
    int temData = 0; // inicializa o contador da data
    std::regex dt ("^# .+/.+/.+"); 

    filein.open(filename, std::ios::app);

    while (!filein.eof()){
        std::getline(filein, mensagem);
        if (mensagem.compare(data) == 0){
            temData++; // incrementa o contador se achar a data
        }
    }
    
    filein.close();

    std::ofstream file;
    file.open(filename, std::ios::app);

    if (temData == 0){
        file << std::endl << data << std::endl;
    }

    for (size_t i = 0; i < diary.messages.size(); i++){
        const Message &messageToWrite = diary.messages[i];

        file << "- ";
        if (messageToWrite.time.hour < 10){
            file << "0"; 
        }
        file << 
        messageToWrite.time.hour << ":"; 
        if (messageToWrite.time.minute < 10){
            file << "0"; 
        }
        file << messageToWrite.time.minute << ":";
        if (messageToWrite.time.second < 10) {
            file << "0";
        } 
        file << messageToWrite.time.second << " " << 
        messageToWrite.content << std::endl;
    }
    file.close();
}

Diary App::readFromFile(std::string filename){

    Diary diaryFile = Diary(filename);    
    
    std::ifstream arquivo;
    arquivo.open(filename, std::ios::app);
    /*
    if (!arquivo.is_open()){
        std::cerr << "O arquivo não pôde ser criado." 
        << std::endl;
        return 1;
    }
    */  
    int counter = 0;
    std::string mensagem;
    std::string date;
    std::string time;
    char discard;
    std::regex dtregex ("^# .+/.+/.+"); 
    
    while (!arquivo.eof()){
        std::getline(arquivo, mensagem);
        if (mensagem.size() == 0){
            continue; // elimina linhas em branco
        }
        counter++;
        bool isData; 
        isData = std::regex_match(mensagem, dtregex);
        if (isData){
            std::stringstream stream(mensagem);
            stream >> discard;
            stream >> date;
            //std::cout << "# " << date << std::endl;
        }
        if (!isData){
            std::stringstream stream(mensagem);
            stream >> discard;
            stream >> time;
            getline(stream, mensagem);
            diaryFile.addFromFile(mensagem, date, time);
        }
       
    }   
    arquivo.close();
    
    
    /* [CHECK] imprimir a lista para ver se deu certo 
    não está na mesma disposição que o markdown, devido
    a estar apenas imprimindos os atributos guardados no Diary */
    /*      
    for (size_t i = 0; i < diaryFile.messages.size(); i++){
        const Message &messageToList = diaryFile.messages[i];
        std::cout << "- " <<
        messageToList.date.day << "/" <<
        messageToList.date.month << "/" << 
        messageToList.date.year << " " <<
        messageToList.time.hour << ":" <<
        messageToList.time.minute << ":" << 
        messageToList.time.second << "" << 
        messageToList.content << std::endl;
    }
    */    
    return diaryFile;
}

void App::search(){
    // if no string was passed in the command line, ask for one
    std::string query;
    // the variable
    std::cout << "What are you looking for?" << std::endl;
    // print a message: user, what do you want me to look for?
    getline(std::cin, query);
    // record the input message into "query"
    App::search(query);
    // use the other search(string) to actually look for the string
}

int App::search(std::string query) {
    Diary temp = readFromFile(diary.filename);
    // read all messages in a diary alread created
    std::vector<Message*> msgExists = temp.search(query);
    // search all these messages

    if(msgExists.size() != 0){
        // if there is a value, then
        for (size_t i = 0; i < msgExists.size(); i++){
            // for each element detected
            Message temp = *msgExists[i];
            // create a temp Message with the element found
            std::cout << "- " << 
            temp.date.to_string() <<
            " " <<
            temp.time.to_string() <<
            temp.content <<
            // print - hour:minute:second and the message itself
            std::endl;
            // move into a new line
        }
    }
    else
    {
        std::cout << "Expression not found." << std::endl;    
        // case msgExists.size == 0, no element was found.
        // Thus, print "not found"
    }

    return 0;
}