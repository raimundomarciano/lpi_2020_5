#include "../include/Diary.h"
#include "../include/addTimeDate.h"
#include "../include/Message.h"
#include <iostream>
#include <fstream>
#include <vector>

Diary::Diary(const std::string &filename) : filename(filename)
{  
    // initializes with a null pointer and receives 
    // an array with size [capacity], as informed above
}

void Diary::add(const std::string &message){
    // add message in the message array
    Message messageToAdd;
    messageToAdd.content = message;
    messageToAdd.date.set_from_string(get_current_date());
    messageToAdd.time.set_from_string(get_current_time());
    
    messages.push_back(messageToAdd);
    
}

void Diary::addFromFile(const std::string &message, const std::string &date, 
const std::string &time){
    // add message in the message array
    Message messageToAdd;
    messageToAdd.content = message;
    messageToAdd.date.set_from_string(date);
    messageToAdd.time.set_from_string(time);
    
    messages.push_back(messageToAdd);
    
}


std::vector<Message*> Diary::search(std::string line){
    std::vector<Message*> msgExists;
    Message* temp;
    for(size_t i = 0; i < messages.size(); i++){
        if(messages[i].content.find(line) != std::string::npos){
            temp = &messages[i];
            msgExists.push_back(temp);
        }
    }
    return msgExists;
}