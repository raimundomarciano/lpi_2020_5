#ifndef DIARY_H
#define DIARY_H
#include "Message.h"    
#include <string>
#include <vector>

struct Diary{
    Diary(const std::string &filename);
 
    std::string filename;
    std::vector<Message> messages; // array that will receive the messages
    //size_t messages_size; // how many elements are in the array
    //size_t messages_capacity; // how many elements can be put into the array
    void add(const std::string &message);
    void addFromFile(const std::string &message, const std::string &date,
    const std::string &time);
    std::vector<Message*> search(const std::string line);
    
};

#endif