#ifndef DIARY_H
#define DIARY_H
#include "Message.h"    
#include <string>

struct Diary{
    Diary(const std::string &filename);
    Diary(const std::string &filename, size_t capacity);
    ~Diary(); // destructor, avoids memory leakage

    std::string filename;
    Message *messages; // array that will receive the messages
    size_t messages_size; // how many elements are in the array
    size_t messages_capacity; // how many elements can be put into the array
    void add(const std::string &message);
    void addFromFile(const std::string &message, const std::string &date,
    const std::string &time);
    Message* search(const std::string line);
    //int extendArray();
};

#endif