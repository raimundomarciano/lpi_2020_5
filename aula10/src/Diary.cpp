#include "../include/Diary.h"
#include "../include/addTimeDate.h"
#include "../include/Message.h"
#include <iostream>
#include <fstream>

Diary::Diary(const std::string &filename) : filename(filename),
messages_capacity(10), messages_size(0), messages(nullptr)
{  messages = new Message[messages_capacity];
    // initializes with a null pointer and receives 
    // an array with size [capacity], as informed above
}

Diary::Diary(const std::string &filename, size_t capacity) : filename(filename),
messages_capacity(capacity), messages_size(0), messages(nullptr)
{  messages = new Message[messages_capacity];
    // initializes with a null pointer and receives 
    // an array with size [capacity], as informed above
}

Diary::~Diary() // implements the destructor
{
     delete[] messages;
}

void Diary::add(const std::string &message){
    // add message in the message array
    Message messageToAdd;
    messageToAdd.content = message;
    messageToAdd.date.set_from_string(get_current_date());
    messageToAdd.time.set_from_string(get_current_time());
    
    messages[messages_size] = messageToAdd;
    messages_size++;

}

void Diary::addFromFile(const std::string &message, const std::string &date, 
const std::string &time){
    // add message in the message array
    Message messageToAdd;
    messageToAdd.content = message;
    messageToAdd.date.set_from_string(date);
    messageToAdd.time.set_from_string(time);
    
    messages[messages_size] = messageToAdd;
    messages_size++;
}


Message* Diary::search(const std::string line){
    Message* msgExists = nullptr;
    for(size_t i=0; i < messages_size; i++)
    {
        if(messages[i].content.find(line) != std::string::npos)
        {
            msgExists = &messages[i];
            i = messages_size;
        }
    }
    return msgExists;
}