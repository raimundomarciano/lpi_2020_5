#include "../include/addTimeDate.h"
#include "../include/Date.h"
#include <ctime>
#include <string>
#include <sstream>

std::string format_current_date(const std::string &format) 
{
  std::time_t time = std::time(nullptr);
  char result[1024];

  std::strftime(result, sizeof(result), format.c_str(), std::localtime(&time));

  return std::string(result);
}

std::string get_current_date() { return format_current_date("%d/%m/%Y"); }
std::string get_current_time() { return format_current_date("%H:%M:%S"); }

Date::Date(): day(0), month(0), year(0){}
Time::Time(): hour(0), minute(0), second(0){}

void Time::set_from_string(const std::string& time)
{
  std::stringstream stream(time);
  char discard;

  stream >> hour;
  stream >> discard;
  stream >> minute;
  stream >> discard;
  stream >> second;

}

void Date::set_from_string(const std::string& date)
{
  std::stringstream stream(date);
  char discard;

  stream >> day;
  stream >> discard;
  stream >> month;
  stream >> discard;
  stream >> year;

}
/*
void Date::to_string(){
  std::string horario;
  
  //26/06/2020
  if (day < 10) {
    horario << 0;
  }
  horario << day;
  
  if (month < 10) {
    stream << 0;
  }
  stream << month;
  stream << discard;
  stream << year;
}*/

std::string Time::to_string(){
  // get the hour/minute/second from a time object and converts into a string
  // hms stands for hour-minute-second
  
  /* in the previous code, this function was not working correctly for I used a 
  stringstream instead of a string */

  std::string hms;
  
  if (hour < 10) {
    hms += "0";
  }
    hms += std::to_string(hour);
    hms += ":";
  
  if (minute < 10) {
     hms += "0";
  }
  hms += std::to_string(minute);
  hms += ":";

  if (second < 10) {
    hms += "0";
  }
  hms += std::to_string(second);

  return hms;
}
