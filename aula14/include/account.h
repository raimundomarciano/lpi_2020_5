#include <iostream>
#include <string>
#include "client.h"

class Conta {
    public:
        Conta(Cliente titular);
        Conta();

        int numeroConta;
        double saldo;
        Cliente titular;

        void saca(int valor);
        void deposita(int valor);
        

};