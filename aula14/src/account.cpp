#include "../include/account.h"

Conta::Conta(Cliente titular) : numeroConta(0), saldo(0), titular(titular){}
Conta::Conta() : numeroConta(0), saldo(0){}

void Conta::saca(int valor){
    saldo -= valor;
}

void Conta::deposita(int valor){
    saldo += valor;
}