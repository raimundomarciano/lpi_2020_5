#include "../include/client.h"

Cliente::Cliente() : nome(""), cpf(0){}

void Cliente::setNome(string novoNome){
    nome = novoNome;
}

string Cliente::getNome(){
    return nome;
}

void Cliente::setCpf(int novoCpf){
    cpf = novoCpf;
}

int Cliente::getCpf(){
    return cpf;
}