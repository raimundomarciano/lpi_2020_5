#include <iostream>
#include <string>
#include <vector>
using namespace std;

class Cliente {
    public:
        Cliente();

        string nome;
        int cpf;

        void setNome(string novoNome);
        string getNome();
        
        void setCpf (int cpf);
        int getCpf();
};
