#include "../include/agency.h"

void Agencia::transfere(int contaOrigem, int contaDestino, double valor){
    rolContas[contaOrigem].saca(valor);
    rolContas[contaDestino].deposita(valor);
}

Conta Agencia::localizaConta(int contaProcurada){
    Conta temp;
    for (size_t i = 0; i < rolContas.size(); i++){
        if (rolContas[i].numeroConta == contaProcurada){
            temp = rolContas[i];
        }
    }
    return temp;
}