#include <iostream>
#include <vector>
#include "../include/agency.h"
using namespace std;

int main(int argc, char* argv[]){
    Cliente eu;
    Cliente tu;
    eu.setNome("Eu");
    tu.setNome("Tu");

    Conta contaEu = Conta(eu);
    Conta contaTu = Conta(eu);
    contaEu.numeroConta = 3;
    cout << "Correntista: " << contaEu.titular.getNome() << endl;
    cout << "Conta N.: " << contaEu.numeroConta << endl;
    cout << "Saldo: " << contaEu.saldo << endl;
    
    cout << endl;

    contaEu.deposita(100);
    cout << "Depósito de R$ 100, seguido de saque de R$ 10"<< endl;
    cout << "Saldo Eu: " << contaEu.saldo << endl;
    contaEu.saca(10);
    cout << "Saldo Eu: " << contaEu.saldo << endl;

    cout << "Teste do vetor de contas na agência" << endl;
    Agencia ag;
    ag.rolContas.push_back(contaEu);
    ag.rolContas.push_back(contaTu);
    cout << "Saldo Eu: " << ag.rolContas[0].saldo << endl;
    cout << "Saldo Tu: " << ag.rolContas[1].saldo << endl;
    cout << "Quantidade de contas: " << ag.rolContas[0].quantidadeContas << endl;
    cout << "Quantidade de clientes: " << eu.quantidadeClientes << endl;

    ag.transfere(0,1,20);
    cout << "Transferência de R$ 20 de 0 para 1" << endl;
    cout << "Saldo Eu: " << ag.rolContas[0].saldo << endl;
    cout << "Saldo Tu: " << ag.rolContas[1].saldo << endl;

    Conta teste = ag.localizaConta(3);
    cout << "Algoritmo de busca pelo número da conta." << endl;
    cout << "Saldo da Conta #3: " << teste.saldo << endl;

    return 0;
}