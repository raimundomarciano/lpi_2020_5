#ifndef CLIENTE
#define CLIENTE

class Cliente{
    double poupanca;
    double cdb;
    
    public:
        Cliente();
        void setPoupanca(double depositoInicial);
        double getPoupanca();
        void setCdb(double depositoInicial);
        double getCdb();
        
};

#endif