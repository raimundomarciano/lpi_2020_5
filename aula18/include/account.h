#include <iostream>
#include <string>
#include "clientepf.h"

class Conta {
    public:
        Conta(Clientepf titular);
        Conta();
        ~Conta();

        int numeroConta;
        double saldo;
        Clientepf titular;
        static int quantidadeContas;

        void saca(int valor);
        void deposita(int valor);

};