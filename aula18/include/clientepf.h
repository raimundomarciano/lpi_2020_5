#ifndef clientepf
#define clientepf

#include <iostream>
#include <string>
#include "cliente.h"
using namespace std;

class Clientepf : public Cliente {
    private:
        string nome;
        string cpf;
    public:
        Clientepf();     
        static int quantidadeClientes;
        void setNome(string novoNome);
        string getNome();
        void setCpf (int cpf);
        string getCpf();
};

#endif