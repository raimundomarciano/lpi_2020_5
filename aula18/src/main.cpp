#include <iostream>
#include <vector>
#include "../include/agency.h"
#include "../include/fila.h"
#include "../include/clientepj.h"
using namespace std;

int main(int argc, char* argv[]){

    Clientepf eles;
    eles.setNome("Eles e Elas");
    eles.setPoupanca(50);
    cout << "Poupança do Cliente " <<eles.getNome() << ": " << eles.getPoupanca() << endl;

    eles.setCdb(350.12);
    cout << "CDB do Cliente " << eles.getNome() << ": " << eles.getCdb() << endl;

    return 0;
}