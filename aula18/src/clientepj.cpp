#include "../include/clientepj.h"
#include <string>

int Clientepj::quantidadeClientesPJ;

Clientepj::Clientepj() : razaoSocial(""), cnpj(""){
    this->quantidadeClientesPJ += 1;
}

void Clientepj::setRazaoSocial(string razao){
    razaoSocial = razao;
}

string Clientepj::getRazaoSocial(){
    return razaoSocial;
}

void Clientepj::setCnpj(string novoCnpj){
    cnpj = novoCnpj;
}

string Clientepj::getCnpj(){
    return cnpj;
}