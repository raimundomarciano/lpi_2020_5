#include "../include/clientepf.h"
#include <string>

int Clientepf::quantidadeClientes;

Clientepf::Clientepf() : nome(""), cpf(""){
    this->quantidadeClientes += 1;
}

void Clientepf::setNome(string novoNome){
    nome = novoNome;
}

string Clientepf::getNome(){
    return nome;
}

void Clientepf::setCpf(int novoCpf){
    cpf = novoCpf;
}

string Clientepf::getCpf(){
    return cpf;
}