#include "../include/Diary.h"
#include "../include/addTimeDate.h"
#include <iostream>

Diary::Diary(const std::string &filename) : filename(filename),
messages_capacity(10), messages_size(0), messages(nullptr)
{  messages = new Message[messages_capacity];
    // initializes with a null pointer and receives 
    // an array with size [capacity], as informed above
}

Diary::~Diary() // implements the destructor
{
     delete[] messages;
}

void Diary::add(const std::string &message){
    // add message in the message array
    Message messageToAdd;
    messageToAdd.content = message;
    messageToAdd.date.set_from_string(get_current_date());
    messageToAdd.time.set_from_string(get_current_time());
    
    messages[messages_size] = messageToAdd;
    messages_size++;

}

void Diary::write(){
    // writes the messages in a file
}