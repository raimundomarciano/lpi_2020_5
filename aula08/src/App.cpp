#include "../include/App.h"
#include "../include/Message.h"
#include <iostream>
#include <fstream>

App::App(const std::string &filename) : diary(filename) {}

int App::run(int argc, char* argv[]) 
{
    if (argc == 1){
        return show_usage();
    }
 
    std::string action = argv[1];

    if (action == "add"){
        if (argc == 2){
            add();
            
        } else {
            add(argv[2]);
        }

    } else if  (action == "list"){
        list_messages();
    } else {
        return show_usage();
    }
    
    return 0;
}

void App::add(){
    std::string message;
    std::cout << "Enter your message: " << std::endl;
    std::getline(std::cin, message);
    diary.add(message);
    App::add_another();
}

void App::add(const std::string message){
    diary.add(message);
    App::add_another();
    //diary.write();
}

void App::add_another(){
    std::cout << "Add another message? (y/n)" << std::endl;
    std::string another = "n";
    std::getline(std::cin, another);
    if (another == "y"){
        App::add();
    } else {
        App::list_messages();
        return;
    }
}

int App::list_messages(){
    for (size_t i = 0; i < diary.messages_size; i++){
        const Message &messageToList = diary.messages[i];
        std::cout << 
        messageToList.date.day << "/" <<
        messageToList.date.month << "/" << 
        messageToList.date.year << " " << 
        messageToList.time.hour << ":" <<
        messageToList.time.minute << ":" << 
        messageToList.time.second << " " << 
        messageToList.content << std::endl;
    }

    // para listar do arquivo
    /*
    std::ifstream arquivo;
    arquivo.open(diary.filename);
    while (!arquivo.eof()){
        std::string messageRecorded;
        std::getline(arquivo, messageRecorded);
        if (messageRecorded.size() == 0){
            continue; // elimina linhas em branco
        }
        std::cout << messageRecorded << std::endl;
    }
    arquivo.close();
    */
    return 0;
}

int App::show_usage(){
    return 1; 
}