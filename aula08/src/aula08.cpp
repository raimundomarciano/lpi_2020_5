#include "../include/App.h"

int main(int argc, char *argv[]){
    App aplicativo("diary.md");
    return aplicativo.run(argc, argv);
}

// g++ -std=c++11 src/aula08.cpp src/App.cpp src/Diary.cpp src/addTimeDate.cpp -o aula08 