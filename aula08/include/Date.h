#ifndef DATE_H
#define DATE_H
#include <string>

struct Date{
    Date();
    // unsigned is a non negative integer
    unsigned day; 
    unsigned month;
    unsigned year;

    // methods are functions inside structs
    void set_from_string(const std::string &date);
};

struct Time{
    Time();
    unsigned hour; 
    unsigned minute;
    unsigned second;

    void set_from_string(const std::string &time);
};

#endif



