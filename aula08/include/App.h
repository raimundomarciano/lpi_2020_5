#ifndef APP_H
#define APP_H

#include "Diary.h"
#include <string>
#include <fstream>

struct App {
    Diary diary;
    App(const std::string &filename); // constructor
    int run(int argc, char* argv[]);
    void add();
    void add(const std::string message);
    int list_messages();
    void add_another();
    int show_usage();
};

#endif