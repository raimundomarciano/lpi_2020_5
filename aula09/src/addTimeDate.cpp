#include "../include/addTimeDate.h"
#include "../include/Date.h"
#include <ctime>
#include <string>
#include <sstream>

std::string format_current_date(const std::string &format) 
{
  std::time_t time = std::time(nullptr);
  char result[1024];

  std::strftime(result, sizeof(result), format.c_str(), std::localtime(&time));

  return std::string(result);
}

std::string get_current_date() { return format_current_date("%d/%m/%Y"); }
std::string get_current_time() { return format_current_date("%H:%M:%S"); }

Date::Date(): day(0), month(0), year(0){}
Time::Time(): hour(0), minute(0), second(0){}

void Time::set_from_string(const std::string& time)
{
  std::stringstream stream(time);
    char discard;

    stream >> hour;
    stream >> discard;
    stream >> minute;
    stream >> discard;
    stream >> second;
}

void Date::set_from_string(const std::string& date)
{
  std::stringstream stream(date);
  char discard;

    stream >> day;
    stream >> discard;
    stream >> month;
    stream >> discard;
    stream >> year;
}

void Date::to_string(){
  std::stringstream stream;
  char discard;

  //26/06/2020
  if (day < 10) {
    stream << 0;
  }
  stream << day;
  stream << discard;
  if (month < 10) {
    stream << 0;
  }
  stream << month;
  stream << discard;
  stream << year;
}

void Time::to_string(){
  std::stringstream stream;
  char discard;
  //05:03:05
  if (hour < 10) {
    stream << 0;
  }
  stream << hour;
  stream << discard; // :
  if (minute < 10) {
    stream << 0;
  }
  stream << minute;
  stream << discard; // :
  if (second < 10) {
    stream << 0;
  }
  stream << second;
}