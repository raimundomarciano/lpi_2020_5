#include "../include/Diary.h"

Diary::Diary(const std::string &filename) : filename(filename),
messages_capacity(10), messages_size(0), messages(nullptr)
{  messages = new Message[messages_capacity];
    // começa inicializado como um ponteiro nulo e agora
    // recebe um array de tamanho [capacity], informado ali em cima mesmo}
}

void Diary::add(const std::string &message){
    // adicionar mensagem no array de mensagens
    Message messageToAdd;
    messageToAdd.content = message;
    // falta incluir os Date e Time no messageToAdd

    messages[messages_size] = messageToAdd;
    messages_size++;

}

void Diary::write(){
    // gravar as mensagens no disco
}