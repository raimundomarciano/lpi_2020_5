#ifndef DATE_H
#define DATE_H
#include <string>

struct Date{
    // unsigned é um inteiro não negativo
    // o int fica implícito, mas poderia ser informado
    unsigned day; 
    unsigned month;
    unsigned year;

    // métodos são funções dentro dos structs
    void set_from_string(const std::string &date);
};

#endif