#ifndef DIARY_H
#define DIARY_H
#include "Message.h"    
#include <string>

struct Diary{
    Diary(const std::string &filename);

    std::string filename;
    Message *messages;
    size_t messages_size; // quantos elementos de fato estao no array
    size_t messages_capacity; // quantos elementos cabem no array
    // checar se precisa ter o add() aqui tb
    void add(const std::string &message);
    void write();
};

#endif