#ifndef TIME_H
#define TIME_H
#include <string>

struct Time{
    // unsigned é um inteiro não negativo
    // o int fica implícito, mas poderia ser informado
    unsigned hour; 
    unsigned minute;
    unsigned second;

    // métodos são funções dentro dos structs
    void set_from_string(const std::string &time);
};

#endif