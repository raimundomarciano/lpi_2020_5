# LPI_2020_5
Repositório para uso na disciplina Linguagens de Programação I.

## Funcionalidades
- <add> adiciona uma mensagem ao diário
- <list> lista as mensagens presentes no arquivo
- <read> carrega as mensagens presentes no arquivo
- <search> busca por mensagens com uma string especificada
- <interactive> inicia um menu com opções
