#include "../include/Config.h"
#include <string>
#include <fstream>
#include <sstream>
#include <iostream>

Config::Config() : path(""), format(""){}
// path and format are blank

void Config::config(){
    std::ifstream configFile;
    std::string configLine;
    std::string temp;
    bool formatExists = false;
    bool pathExists = false; 
    
    configFile.open("diary.config", std::ios::app);   
    while (!configFile.eof()){

        std::getline(configFile, temp);
        if (temp.size() == 0){
            continue; // elimina linhas em branco
        }

        
        if (temp.substr(0,4) == "path"){
            path += temp.substr(5,temp.length() - 5);
            // path= (5 characteres)
            pathExists = true;
        }

        if (temp.substr(0,14) == "default_format"){
            format += temp.substr(15,temp.length() - 15);
            //default_format= (15 chars)
            formatExists = true;
        }

    }   
    
    configFile.close();

    if (formatExists == false && pathExists == false) {
        // if both are false, then no parameters were found
        std::ofstream configFile;
        configFile.open("diary.config", std::ios::trunc);
            configFile << "path=diary.md" << std::endl;
            configFile << "default_format=%d %t: %m" << std::endl;
        configFile.close();
        format += "%d %t: %m";
        path += "diary.md";
    }
}