#include "../include/App.h"
#include "../include/Diary.h"
#include "../include/Config.h"

int main(int argc, char *argv[]){
    Config configuration;
    configuration.config();
    App aplicativo(configuration.path);
    return aplicativo.run(argc, argv);
}
