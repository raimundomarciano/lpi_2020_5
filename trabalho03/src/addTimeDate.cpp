#include "../include/addTimeDate.h"
#include "../include/Date.h"
#include <ctime>
#include <string>
#include <sstream>

std::string format_current_date(const std::string &format) {
  std::time_t time = std::time(nullptr);
  char result[1024];

  std::strftime(result, sizeof(result), format.c_str(), std::localtime(&time));

  return std::string(result);
}

std::string get_current_date() { return format_current_date("%d/%m/%Y"); }
std::string get_current_time() { return format_current_date("%H:%M:%S"); }

Date::Date(): day(0), month(0), year(0){}
Time::Time(): hour(0), minute(0), second(0){}

void Time::set_from_string(const std::string& time){
  std::stringstream stream(time);
  char discard;

  stream >> hour;
  stream >> discard;
  stream >> minute;
  stream >> discard;
  stream >> second;

}

void Date::set_from_string(const std::string& date){
  std::stringstream stream(date);
  char discard;

  stream >> day;
  stream >> discard;
  stream >> month;
  stream >> discard;
  stream >> year;

}

std::string Date::to_string(){
  // get the day/month/year from a time object and converts into a string
  // dmy stands for day-month-year
  std::string dmy;
  if (day < 10) {
    dmy += "0";
  }
    dmy += std::to_string(day);
    dmy += "/";
  
  if (month < 10) {
     dmy += "0";
  }
  dmy += std::to_string(month);
  dmy += "/";

  dmy += std::to_string(year);

  return dmy;
}

std::string Time::to_string(){
  // get the hour/minute/second from a time object and converts into a string
  // hms stands for hour-minute-second
  
  std::string hms;
  
  if (hour < 10) {
    hms += "0";
  }
    hms += std::to_string(hour);
    hms += ":";
  
  if (minute < 10) {
     hms += "0";
  }
  hms += std::to_string(minute);
  hms += ":";

  if (second < 10) {
    hms += "0";
  }
  hms += std::to_string(second);

  return hms;
}

