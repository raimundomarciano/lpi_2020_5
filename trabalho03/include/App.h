#ifndef APP_H
#define APP_H

#include "Diary.h"
#include "Config.h"
#include <string>
#include <fstream>

struct App {
    Diary diary;
    Config configuration;
    App(const std::string &filename); // constructor
    int run(int argc, char* argv[]);
    void add();
    void add(const std::string message);
    int show_usage();
    void write(std::string filename);
    Diary readFromFile(std::string filename);
    void list();
    void list(std::string format);
    void search();
    int search(std::string query);
    void interactive();
};

#endif