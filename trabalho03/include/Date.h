#ifndef DATE_H
#define DATE_H
#include <ctime>
#include <string>

struct Date{
    Date();
    unsigned day; 
    unsigned month;
    unsigned year;

    void set_from_string(const std::string &date);
    std::string to_string();
};

struct Time{
    Time();
    unsigned hour; 
    unsigned minute;
    unsigned second;

    void set_from_string(const std::string &time);
    std::string to_string();
};

#endif