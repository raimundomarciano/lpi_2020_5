#ifndef CONFIG_H
#define CONFIG_H
#include <string>

struct Config {
    Config();
    std::string path;
    std::string format;
    void config();
};



#endif