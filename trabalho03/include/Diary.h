#ifndef DIARY_H
#define DIARY_H
#include "Message.h"    
#include <string>
#include <vector>

struct Diary{
    Diary(const std::string &filename);
 
    std::string filename;
    std::vector<Message> messages; // array that will receive the messages
    void add(const std::string &message);
    void addFromFile(const std::string &message, const std::string &date,
    const std::string &time);
    std::vector<Message*> search(const std::string line);
    
};

#endif